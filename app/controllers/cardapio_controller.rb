# -*- coding: utf-8 -*-
class CardapioController < ApplicationController

  def index
    require 'nokogiri'
    require 'open-uri'

    doc = Nokogiri::HTML(open('http://www.ufrgs.br/ufrgs/aluno'))

    t = Time.now.in_time_zone
    wday = t.wday
    if wday == 0
      wday = 1
      t = t + 1.day
    end
    if wday == 6
      wday = 1
      t = t + 2.days
    end
    wday = wday-1

    cardapio_day = doc.css("#ru #carousel #slideshow #slidesContainer .slide.dia h3")
    cardapio = doc.css("#ru #carousel #slideshow #slidesContainer .slide.dia div")

    if cardapio_day.length <= wday
      render :text => "<div style='text-align:center;'><div><b>#{t.strftime('%d/%m/%Y')}</b></div>Cardápio não definido para este dia.</div>"
    else
      cardapio_s = "<div style='text-align:center;'><div><b>#{cardapio_day[wday].content} (#{t.strftime('%d/%m/%Y')})</b></div>"+cardapio[wday].to_s+"</div>"
      render :text => cardapio_s
    end

  end

  def all
    require 'nokogiri'
    require 'open-uri'

    doc = Nokogiri::HTML(open('http://www.ufrgs.br/ufrgs/ru'))

    cardapio_ru = doc.css("#cardapio .ru")

    dict = {}

    cardapio_ru.each do |ru|
      key = ru.css("h3")[0].content
      value = "<div style='text-align:center;'><div><b>#{key}</b></div>"+ru.css(".area")[0].to_s+"</div>"
      dict[key] = value
    end

    #cardapio_day = doc.css("#ru #carousel #slideshow #slidesContainer .slide.dia h3")
    #cardapio = doc.css("#ru #carousel #slideshow #slidesContainer .slide.dia div")
=begin
    dict = { }

    t = Time.now.in_time_zone
    wday = t.wday
    if wday == 0
      wday = 1
      t = t + 1.day
    end
    if wday == 6
      wday = 1
      t = t + 2.days
    end
    wday = wday-1


    (0..4).each do |wdayb|

      nd = wdayb - wday
      b = t + nd.days

      if cardapio_day.length <= wdayb
        dict[wdayb] = "<div style='text-align:center;'><div><b>#{b.strftime('%d/%m/%Y')}</b></div>Cardápio não definido para este dia.</div>"
      else
        dict[wdayb] = "<div style='text-align:center;'><div><b>#{cardapio_day[wdayb].content} (#{b.strftime('%d/%m/%Y')})</b></div>"+cardapio[wdayb].to_s+"</div>"
      end

    end
=end

    render :text => [dict].to_json

  end

end
